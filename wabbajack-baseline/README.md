# Skyrim LE TIVO modding baseline

These are the files you need to install our modding baseline. This is a very general baseline and can be used for your own modlist.

Open `Skyrim TIVO.wabbajack` in Wabbajack and everything should be installed for you.
