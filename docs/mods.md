## MODS

This modlist would not be possible without these great resources:

* [Skyrim Ultimate Modding Guide - Graphics](https://www.sinitargaming.com/skyrim_graphics.html)
* [Skyrim Ultimate Modding Guide - Gameplay](https://www.sinitargaming.com/skyrim_gameplay.html)
* [Modding Skyrim](https://moddingskyrim.com/)
* [STEP Modifications](https://stepmodifications.org/)

### Additional links

* [Ordenador User Guide and Full Config](https://www.nexusmods.com/skyrim/mods/71294)

## Modding Tools

* [LOOT](https://loot.github.io). It will sort all your mods in the correct load order and warn you if it sees problems like missing dependencies or compatibility-patches.
* [TES5Edit](https://www.nexusmods.com/skyrim/mods/25859). Required to clean mods and a few other things.
* [zEDIT](https://z-edit.github.io/). The successor to xEdit (TES5Edit).
* [Wrye Bash](https://www.nexusmods.com/skyrim/mods/1840). Required to create compatibility patches and merge mods.

### Modding baseline

Mods that are required for (almost) every modded game.

* [SKSE](http://skse.silverlock.org/). The Skyrim Script Extender is a dependency for a lot of mods.
* [SKSE ini pre-download for lazy users](https://www.nexusmods.com/skyrim/mods/51038)
* [SkyUI](http://www.nexusmods.com/skyrim/mods/3863). The Skyrim UI was designed for consoles, and SkyUI fixes that. A lot of mods require it.
* [Crash Fixes](https://www.nexusmods.com/skyrim/mods/72725). Fixes several crashes of the game engine.
* [SKSE Plugin Preloader](https://www.nexusmods.com/skyrim/mods/75795). Definately required.
* [FileAccess Interface for Skyrim Script - FISS](https://www.nexusmods.com/skyrim/mods/48265)
* [Bug fixes](https://www.nexusmods.com/skyrim/mods/76747). Another bugfix mod. Download the *Bug Fixes v2 Beta 2* file.
* [Cobb Bug Fixes](https://www.nexusmods.com/skyrim/mods/96734). And some more general bugfixes.
* [PapyrusUtil - Modders Scripting Utility Functions](https://www.nexusmods.com/skyrim/mods/58705)
* [Unofficial Skyrim Legendary Edition Patch](https://www.nexusmods.com/skyrim/mods/71214)
* [Skyrim Supplemental Patch](https://www.nexusmods.com/skyrim/mods/91069)
* [Unofficial Skyrim City Patch (USCP) Reawakened](https://www.nexusmods.com/skyrim/mods/108042)
* [Load Game CTD Fix](https://www.nexusmods.com/skyrim/mods/85443)
* [Grid Transition CTD Fix](https://www.nexusmods.com/skyrim/mods/108260)
* [15 minute crash fix for Windows 10 and 11](https://www.nexusmods.com/skyrim/mods/90040). *OPTIONAL* Only install/enable if required. Fixes the annoying crash after exactly 15 minutes of playing due to an annoying Windows 10, 11 service conflict.
* [1st Person Candlelight Fix](https://www.nexusmods.com/skyrim/mods/83608). Do not let *any* mod overwrite the files of this mod!
* [Better Dialogue Controls](https://www.nexusmods.com/skyrim/mods/27371)
* [Better MessageBox Controls](https://www.nexusmods.com/skyrim/mods/28170)
* [Extended UI](https://www.nexusmods.com/skyrim/mods/57873)
* [Atlas Map Markers for Skyrim](https://www.nexusmods.com/skyrim/mods/14976)
* [Atlas Map Markers - Updated with MCM](https://www.nexusmods.com/skyrim/mods/74045)
* [NPC AI Behavior Fixes - Plugin](https://www.nexusmods.com/skyrim/mods/108301)
* [Modern Toggle Walk-Run Fix LE](https://www.nexusmods.com/skyrim/mods/106346)
* [Equip Enchantment Fix](https://www.nexusmods.com/skyrim/mods/105666)
* [Dawnguard Soul Cairn Microsoft Visual Runtime Error R6025 Fix](https://www.nexusmods.com/skyrim/mods/41411)
* [Assorted Mesh Fixes LE](https://www.nexusmods.com/skyrim/mods/109575)
* [Lightened Skyrim LE](https://www.nexusmods.com/skyrim/mods/108047)
* [RaceMenu](https://www.nexusmods.com/skyrim/mods/29624)
* [Player Rotation in ShowRaceMenu](https://www.nexusmods.com/skyrim/mods/102937)
* [Smoother Skies](https://www.nexusmods.com/skyrim/mods/109709)
* [Dirtcliff Texture Fix](https://www.nexusmods.com/skyrim/mods/95645)
* [Sovngarde Particle Fix](https://www.nexusmods.com/skyrim/mods/95361)
* [Better Jumping](https://www.nexusmods.com/skyrim/mods/65044)
* [Barter Speechcraft Experience Fix](https://www.nexusmods.com/skyrim/mods/102855)
* [Proper Stagger Direction](https://www.nexusmods.com/skyrim/mods/68282)
* [Mfg Console](https://www.nexusmods.com/skyrim/mods/44596)
* [Crash Log](https://www.nexusmods.com/skyrim/mods/106929)
* [Weapons and Armor Fixes Remade](https://www.nexusmods.com/skyrim/mods/34093) - Compatibility patches!
* [Clothing and Clutter Fixes](https://www.nexusmods.com/skyrim/mods/43053)
* [Complete Crafting Overhaul Remade](https://www.nexusmods.com/skyrim/mods/49791) - Also adds support for a few other mods!
* [Wiseman303's Flora Fixes](https://www.nexusmods.com/skyrim/mods/70656)
* [Wiseman303's Critter Fixes](https://www.nexusmods.com/skyrim/mods/54485)
* [Better Harvesting](https://www.nexusmods.com/skyrim/mods/75511)
* [Inconsequential NPCs](https://www.nexusmods.com/skyrim/mods/36334)
* [Citizens of Tamriel](https://www.nexusmods.com/skyrim/mods/99406)
* [Inconsequential NPCs Visual Overhaul](https://www.nexusmods.com/skyrim/mods/103012)
* [Citizens of Tamriel Visual Overhaul](https://www.nexusmods.com/skyrim/mods/106320)
* [Skyrim -Community- Uncapper](https://www.nexusmods.com/skyrim/mods/1175)
* [Fuz Ro D-oh - Silent Voice](https://www.nexusmods.com/skyrim/mods/14884)
* [Unequip Quiver](https://www.nexusmods.com/skyrim/mods/106105)
* [Dynamic Animation Replacer](https://www.nexusmods.com/skyrim/mods/101371)
* [Difficulty Balance](https://www.nexusmods.com/skyrim/mods/98605)
* [Optional Quick Start](https://www.nexusmods.com/skyrim/mods/111029). Simply enables you to skip the intro. Most lightweight mod that skips the intro.

Congratulations! With these mods you have a solid foundation for further modding... or a Vanilla Skyrim gameplay experience without most bugs and crashes.

### Baseline extended

These mods are supported by CCOR (Complete Crafting Overhaul Remade) and don't require their ESP's to be enabled. In game, enable support for these in CCOR.

* [Bandolier - Bags and Pouches](https://www.nexusmods.com/skyrim/mods/16438)
* [Cloaks of Skyrim](https://www.nexusmods.com/skyrim/mods/12092)
* [JaySuS Swords](https://www.nexusmods.com/skyrim/mods/1002)
* [Winter Is Coming - Cloaks](https://www.nexusmods.com/skyrim/mods/13486)

And these mods we don't want missing in out load order either.

* [aMidianBorn Book of Silence](https://www.nexusmods.com/skyrim/mods/24909)
* [Improved closefaced helmets](https://www.nexusmods.com/skyrim/mods/15927) - Compatibility patches!
* [Helgen Reborn](https://www.nexusmods.com/skyrim/mods/35841)
* [Immersive Armors](https://www.nexusmods.com/skyrim/mods/19733)
* [Immersive Weapons](https://www.nexusmods.com/skyrim/mods/27644)
* [Wet and Cold](https://www.nexusmods.com/skyrim/mods/27563)
* [Guard Dialogue Overhaul](https://www.nexusmods.com/skyrim/mods/23390)
* [Even Better Quest Objectives - EBQO](https://www.nexusmods.com/skyrim/mods/32695) - Compatibility patches!

### Animation

#### Nemesis Unlimited Behavior Engine

* [Project New Reign - Nemesis Unlimited Behavior Engine](https://www.nexusmods.com/skyrim/mods/87642)
* [Slow sprint bug fix](https://www.nexusmods.com/skyrim/mods/109519)

#### Dynamic Animation Replacer
* [Dynamic Animation Replacer](https://www.nexusmods.com/skyrim/mods/101371) has already been installed as a baseline mod.


