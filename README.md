# Skyrim LE TIVO

Skyrim Legendary Edition - TIVO (The Immersive Visual Overhaul) is a modlist specifically for low-end rigs. It consists of mods that increase immersion and gameplay, fixes many bugs and replaces a vast number of textures with 1k optimized versions to improve performance.

Some of the functionality that will be added or improved compared to the vanilla game:

* Better and performance friendly textures
* Better meshes for improved graphics
* Improved visuals and player interaction for NPC's
* Improved animations
* Improved but lightweight combat system, spells, perks, skills, etc.
* Improved player character visuals
* Lightweight survival system with more interesting crafting, spell-learning, etc.
* Plenty of bugfixes for a better gaming experience

All of this and more will create a RPG game that lets you play your character as if truely part of the world of Skyrim, all while remaining lore-friendly and as true to the spirit of the vanilla game as possible. As we consider this modlist 'Vanilla+', it will stay below the 256 ESP threshold, so no merged patches or bashed batches are needed.

## Installing

In our [wiki](https://gitlab.com/skyty-modding/skyrim-tivo/-/wikis/home) you will find all instructions to get you started. Installation can be done automatically through Wabbajack or manually through detailed instructions in our wiki.

## Intended audience
This mod list is intended for gamers with a low-end to medium gaming rig. It was developed and tested on the following hardware:

* Intel Core i7-3632QM CPU @ 2.20GHz
* 8GB RAM
* NVIDIA GeForce 710M (2GB RAM) graphics card

If you have similar specs or higher, you should be able to run our Skyrim LEmod collection with a reasonable framerate. It may even work with lower specs, but I'm unable to test that. Hopefully, others with similar rigs are willing to playtest the game and help improve this project.

## Linux

Although it is quite possible to run Skyrim on Linux modding it is another matter entirely. I've been able to do it with Vortex, which is also available on Lutris, but it's another beast entirely. Some mods simply won't work on Linux, due to required software or libraries. You may use the guide of this mod collection to get a similar setup, but that is way beyond the scope of our guide.
